@extends('layouts.master')
@section('title', 'All Cast')
@section('content')
  <a href="/cast/create" class="btn btn-primary my-3">Add Cast</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Umur</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $keys=>$key_cast)
                <tr>
                    <th scope="row">{{ $keys + 1 }}</th>
                    <td>{{ $key_cast->name }}</td>
                    <td>{{ $key_cast->umur }}</td>
                    <td>
                        <form action="/cast/{{ $key_cast->id }}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/cast/{{ $key_cast->id }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{ $key_cast->id }}/edit" class="btn btn-warning btn-sm">Update</a>
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Cast kosong!</td>
                </tr>
            @endforelse

        </tbody>
    </table>
@endsection
