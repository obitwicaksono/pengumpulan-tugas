@extends('layouts.master')
@section('title', 'All Cast')
@section('content')
    <h1 class="text-primary">{{$cast->name}}</h1>
    <p>{{$cast->bio}}</p>
    <a href="/cast" class="btn btn-primary btn-sm my-3">Back</a>
@endsection
