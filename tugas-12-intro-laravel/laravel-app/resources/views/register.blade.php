<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="/welcome" method="POST">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <p>
            <label>First name:</label> <br>
            <input type="text" name="firstName" />
        </p>
        <p>
            <label>Last name:</label> <br>
            <input type="text" name="lastName" />
        </p>
        <p>
            <label>Gender:</label> <br><br>
            <label><input type="radio" name="gender" value="male" /> Male</label> <br>
            <label><input type="radio" name="gender" value="female" /> Female</label>
        </p>
        <p>
            <label>Nationality:</label> <br><br>
            <select name="nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="American">American</option>
                <option value="Spanish">Spanish</option>
                <option value="Colombian">Colombian</option>
                <option value="Other">Other</option>
            </select>
        </p>
        <p>
            <label>Language Spoken:</label> <br><br>
            <input type="checkbox" value="Bahasa Indonesia">
            <label>Bahasa Indonesia</label><br>
            <input type="checkbox" value="English">
            <label>English</label><br>
            <input type="checkbox" value="Other">
            <label>Other</label><br>
        </p>
        <p>Bio:</p>
        <p>
            <textarea name="bio"></textarea>
        </p>
    
        <p> <br>
            <input type="submit" value="Sign Up">
        </p>
    </form>
</body>
</html>